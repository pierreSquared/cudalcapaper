\documentclass{article}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{placeins}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage[utf8]{inputenc}
\usepackage{xcolor}
\usepackage{hyperref} 
\usetikzlibrary{arrows}

\DeclareMathOperator{\inlabel}{Inlabel}
\DeclareMathOperator{\LCA}{LCA}
\DeclareMathOperator{\level}{Level}
\DeclareMathOperator{\ascendant}{Ascendant}
\DeclareMathOperator{\head}{Head}
\DeclareMathOperator{\preorder}{preorder}
\DeclareMathOperator{\inorder}{inorder}
\DeclareMathOperator{\parent}{Parent}
\DeclareMathOperator{\size}{Size}
\DeclareMathOperator{\Wone}{W_1}
\DeclareMathOperator{\Wtwo}{W_2}
\DeclareMathOperator{\Wthree}{W_3}
\DeclareMathOperator{\dOne}{Dist_1}
\DeclareMathOperator{\dTwo}{Dist_2}
\DeclareMathOperator{\dThree}{Dist_3}

\definecolor{c1}{HTML}{8D75A0}
\definecolor{c2}{HTML}{8B9CD6}
\definecolor{c3}{HTML}{F4F1BB}
\definecolor{c4}{HTML}{DB9D47}
\definecolor{c5}{HTML}{FF784F}

% \definecolor{c1}{HTML}{D6F7A3}
% \definecolor{c2}{HTML}{C4FFB2}
% \definecolor{c3}{HTML}{B7E3CC}
% \definecolor{c4}{HTML}{7D82B8}
% \definecolor{c5}{HTML}{7B4B94}

\graphicspath{ {./img/} }

\tikzset{
  treenode/.style = {align=center, inner sep=0pt, text centered, font=\sffamily},
  arn_n/.style = {treenode, circle, black, font=\sffamily\bfseries, draw=black, text width=1.5em},
  in8/.style = {arn_n, fill=c3},
  in6/.style = {arn_n, fill=c4},
  in7/.style = {arn_n, fill=c5},
  in3/.style = {arn_n, fill=c1},
  in2/.style = {arn_n, fill=c2},
  empty/.style = {treenode, white, draw=white}
}

\author{Adrian Siwiec}
\date{\today{}}
\begin{document}
\begin{titlepage}
    \begin{center}
        
        \large
        \textbf{Jagiellonian University}\\
        Department of Theoretical Computer Science\\

        \vspace{1.5cm}

        \Large
        \textbf{Adrian Siwiec}

        \vspace*{2cm}

        \textbf{\LARGE Solving LCA Problem on GPU \\ \Large \mbox{Practical Algorithm with Theoretical Guarantees}}
        
        \vspace{0.5cm}
        \large
        
        \vfill
        \Large
        Bachelor Thesis

        \vfill
        \Large
        Supervisor: dr Maciej Ślusarek
        
        \vspace{0.8cm}
        
        June 2018
        
    \end{center}
\end{titlepage}


\pagebreak

\begin{abstract}
The common knowledge is that classic PRAM graph algorithms do not fit well the GPU architecture. We show that it is not necessarily the case and investigate an Euler tour based algorithm for parallel LCA calculation on a modern GPU. Our experiments prove that the theoretically optimal PRAM algorithm is on par with a simple na\"ive solution on average, and outperforms it considerably in the worst-case scenarios.
\end{abstract}

\textbf{Supplementary materials:} \url{https://github.com/AdrianSiwiec/CudaLCA}

\section{Introduction}
One of the most common problems on trees is how to find the lowest common ancestor (LCA) of two nodes. Given a rooted tree T(V, E), a node $z\in T$ is a common ancestor of nodes $x$ and $y$ if it is an ancestor of $x$ and an ancestor of $y$. The lowest common ancestor of $x$ and $y$ is the common ancestor of $x$ and $y$ whose distance from root is the largest possible.

We will denote the lowest common ancestor of $x$ and $y$ by $\LCA(x, y)$, the number of nodes $|V|$ by $n$ and a root of $T$ by $r$.

The LCA problem is given as follows: given a rooted tree $T$ and a sequence of queries $(u_1, v_1), (u_2, v_2), \ldots, (u_m, v_m)$ calculate the sequence $w_1, w_2, \ldots, w_m$, where $w_i=\LCA(u_i, v_i)$.

It is a very popular problem and has been extensively studied. It appears as a subproblem while solving many other graph problems, for example: minimum spanning tree \cite{KK95:1}, dominator tree in a directed flow-graph \cite{AH76:1}, maximum weighted matching \cite{G90:1}, network routing \cite{TZ01:1}, phylogenetic distance computation \cite{MT12:1}, etc.

Even though computers get faster, the problems we are trying to solve using them grow in size and complexity even more. In recent times the progress of CPUs has slowed down with more attention given to parallel computing machines such as GPUs.

Many PRAM algorithms were invented before creation of the GPUs. When parallel computing on GPUs gained popularity it then became apparent that in practice most of the algorithms designed for a PRAM model, while being theoretically fast, are slow and unusable on GPU. While GPU seems to be compatible with PRAM model, it suffers from many limitations not considered before such as: costly communication, synchronization and significant memory latency.

What is surprising is that the \emph{Euler tour} technique, while being one of the most commonly used building blocks for graph and tree algorithms for PRAM, is rarely used for calculations on GPU. Na\"ive algorithms are often used instead \cite{MT12:1}.

In this thesis we argue that Euler tour-based algorithms can have their place and use cases on modern GPUs. We discuss the problem of finding LCA in trees. We benchmark theoretically optimal Euler-tour-based GPU algorithm against a na\"ive GPU algorithm, and a CPU baseline.


\section{Algorithms}

There are many algorithms for solving LCA problem. In 1984 Harel and Tarjan \cite{HT84:1} showed an algorithm that required only $O(n)$ time for preprocessing and answered queries in constant time per query. However, this algorithm was quite complicated. In 1988 Schieber and Vishkin \cite{SV88:1} showed an algorithm that ran in the same time, while being a little bit simpler and having a parallel version for a PRAM machine.

Michael A. Bender and Mart{\'{\i}}n Farach-Colton \cite{BC00} showed an algorithm that reduces the LCA problem to the $\pm1\text{RMQ}$ problem and solves it using linear time for preprocessing and constant time per query. A very common and simple version of this method uses linear-time reduction to the RMQ problem and then answers queries in $O(\lg n)$ time per query.

\subsection{Na\"ive Algorithm for GPU}
\label{s:NaiveGPU}
A commonly used na\"ive LCA algorithm for GPU answers queries by traversing the tree upwards from both nodes of a query. To avoid writing to large global ``visited'' table, which is costly on GPU, it employs simple preprocessing. The algorithm calculates node levels in preprocessing phase, where for $x\in T$, $\level(x)$ is equal to the length of the path from $r$ to $x$.

The array $\level$ is calculated by using standard pointer jumping algorithm: for each node $x\in T, x\neq r$ there is a pointer to a parent and a temporary $\level$, at first equal to zero. Then in parallel each pointer is moved to a parent of a parent and the $\level$ is updated accordingly. This procedure is repeated until all pointers point at $r$. This algorithm has $O(\lg{n})$ time complexity with work of $O(n\lg{n})$. After preprocessing, for each query $(x, y)$ there is a pair of pointers, at first pointing at $x$ and $y$. Then, the pointer with higher $\level$ is moved up, until both pointers are at the same level. Next, both pointers move upwards until they meet at the $\LCA(x, y)$.

This algorithm has advantage of being simple, with fast preprocessing and fast query times for shallow trees. It has however a major problem -- answering queries in deep trees can take up to linear time for a single query.

\subsection{Inlabel Algorithm}
In 1988 Schieber and Vishkin \cite{SV88:1} showed an algorithm, that runs preprocessing in $O(n)$ time and answers queries in $O(1)$ time on a RAM machine. The algorithm can be easily parallelized. The PRAM version runs preprocessing in $O(\lg{n})$ time using $n$ processors, and calculates queries in $O(1)$ time using a single processor per query. We will call it \emph{inlabel algorithm}.

Below we outline the algorithm without going into much details or providing proofs. For more detailed description see work of Schieber and Vishkin \cite{SV88:1}.
\subsubsection{Preprocessing}
\label{s:InlabelPreproc}
The preprocessing stage of inlabel algorithm computes three labels for each node $x\in T$: $\level(x)$, $\inlabel(x)$ and $\ascendant(x)$ with an additional look-up array called $\head$.

For $x\in T, \level(x)$ is equal to the length of the path from $x$ to $r$, with $\level(r)=0$.

We want inlabel numbers to satisfy two conditions: 

\begin{enumerate}
\item \emph{Path partition property}. Inlabel numbers partition $T$ into paths going top-down, each path created from nodes having same inlabel number.

\item \emph{Inorder property}. Let $B$ be the smallest full binary tree having at least $n$ nodes. We identify each node of $B$ with its \emph{inorder} number. Inlabel maps nodes from our tree $T$ to the nodes of $B$ (it is not usually an injection) in such a way, that for every $x, y\in T$, if $x$ is a descendant of $y$ (in $T$) than $\inlabel(x)$ is a descendant of $\inlabel(y)$ (in $B$).
\end{enumerate}

Let us consider the \emph{preorder} numbering of nodes of $T$. By $\size(x)$ we denote the size of the subtree of $x$. All nodes of this subtree have preorder numbers within the closed interval $[\preorder(x), \preorder(x)+\size(x)+1]$. Let us call this interval the \emph{interval of x}.

For each $x\in T$ let us look at the binary representations of numbers in the interval of $x$. Let $\inlabel(x)$ be the number which has the most trailing zeros.

Let us notice that, because of preorder numbering of T, inlabel defined in such way satisfies the path partition property, and, because of characteristics of inorder numbering of full binary trees, inlabel also satisfies the inorder property.

The idea behind $\ascendant(x)$ is to encode inlabel numbers of all ancestors of $x$. Note that such compact encoding is at all possible, because on a path from $x$ to $r$, every $\inlabel$ is a prefix of $\inlabel(x)$, followed by a single ``1'' and some number of trailing ``0''s. Therefore, from a point of view of $x$, it is sufficient to encode the right-most ``1'' of each inlabel.

 Let us assign $\ascendant(r):=\inlabel(r)$ and continue top-down. For $x\in T$ let $p$ be the parent of $x$. If $\inlabel(x)=\inlabel(p)$ we set $\ascendant(x)$ to be equal to $\ascendant(p)$. Otherwise, let us assign $\ascendant(x)$ to be equal to $\ascendant(p)+2^i$, where $i$ is the index of the right-most ``1'' in binary representation of $\inlabel(x)$.

Finally, let $\head(i)$ be the preorder number of the highest node on the path defined by $i$. (Note, that $i$ is the lowest node on this path.)

\subsubsection{Answering Queries}

\begin{figure}[ht]
\begin{minipage}[b]{.5\textwidth}
\begin{tikzpicture}[level/.style={sibling distance = 7.1cm/#1,
  level distance = 1cm},
  level 1/.style={sibling distance=70mm},
  level 2/.style={sibling distance=25mm},
  level 3/.style={sibling distance=23mm},
  level 4/.style={sibling distance=13mm}] 
\node[empty]{aa}
child{ node(t1)[label=left:$\mathit{\hat{x}, z}$][in8] {1} edge from parent[draw=none]
    child{ node(t2)[label=left:$\head(w)$][in2] {2} 
            child{ node(t3)[label=left:$x$][in3] {3}}
	}
    child{ node(t4)[label=left:$\hat{y}$][in8] {4}   
            child{ node(t5)[in6] {5}
				child{ node(t6)[label=left:$y$][in6] {6}}
				child {node(t7)[in7] {7}}
			}
			child{ node(t8)[in8]{8}}                                 
    }
}
child {node(b8)[in8] {8} edge from parent[draw=none]
    child{ node[label=left:$b$][arn_n] {4} 
            child{ node(b2)[in2]{2} 
            	child{ node[arn_n] {1}}
				child{ node(b3)[label=left:$x$][in3] {3}}
            }
            child{ node(b6)[label=left:$y$][in6] {6}
							child{ node [arn_n] {5}}
							child{ node(b7)[in7] {7}}
            }                            
    }
    child{ node {...} }
    }
; 

% \path[bend left, ->, dotted]
%     (t1) edge node [right] {} (b8)
%     (t4) edge node [right] {} (b8)
%     (t8) edge node [right] {} (b8)
%     (t5) edge [bend left] node [right] {} (b6)
%     (t6) edge [bend right] node [right] {} (b6)
%     (t2) edge [bend right] node [right] {} (b2)
%     (t3) edge [bend right] node [] {} (b3)
%     (t7) edge [bend right] node [] {} (b7)
%     ;

\end{tikzpicture}
\end{minipage}
\caption{Inlabel mapping from T to B}
\label{f:InMapping}
\end{figure}

For any pair of nodes $x, y\in T$ we briefly show how to calculate $z:=\LCA(x, y)$.

When $\inlabel(x)=\inlabel(y)$, $x$ and $y$ are on a same path to root, so $z$ is equal to the one with the lower $\level$.

Otherwise we find $\LCA(x,y)$ in four steps. First, we find $b:=\LCA(\inlabel(x)$, $\inlabel(y)$) in the complete binary tree $B$ (defined above). It is simple and can be done in a constant time, using bit operations.

Then, we find $\inlabel(z)$. Note, that it does not have to equal $b$, because it is possible that none of the nodes on paths from $x$ and $y$ to $r$ have $\inlabel$ equal to $b$. Using $\ascendant$ array we look for first $\inlabel$ number common for $x$ and $y$ that is an ancestor of $b$. This again can be done in $O(1)$ time 
using bit operations.

Inlabel($z$) defines a path in $T$. We want to find $\hat{x}$ and $\hat{y}$ being the first ancestors of $x$ and $y$, respectively, on a path defined by $\inlabel(z)$ (see Figure \ref{f:InMapping}). To find $\hat{x}$ we decode $w$ equal to the last $\inlabel$ number that comes before $\inlabel(z)$. We do this using $\ascendant(x)$. When we have $w$, in array $\head(w)$ we find head of path defined by $w$. Therefore $\hat{x}$ is equal to the parent of $\head(w)$. We find $\hat{y}$ in a similar manner.

$\LCA(x, y)$ is equal to $\hat{x}$ if $\level(\hat{x})<\level(\hat{y})$ and to $\hat{y}$ otherwise.

\section{Inlabel GPU Implementation}

The source code of the implementation, along with testing scripts is available at \href{https://github.com/AdrianSiwiec/CudaLCA}{https://github.com/AdrianSiwiec/CudaLCA}

\subsection{High Level Overview}

We take parent array as an input, which describes a parent of each node in $T$. We then construct a list of edges directed from parents and sort it lexicographically. From such sorted list we get an \emph{Euler tour representation (ETR)} of our graph, in a way similar to one shown in a work of Tarjan and Vishkin \cite{TV84:1}.

For each node $x\in T, x\neq r$ let us denote the parent of $x$ by $p_x$. Then, we assign weights $W_1$ and $W_2$ to edges in ETR, as follows. $W_1(p_x\rightarrow x)=1$, $W_1(x\rightarrow p_x)=0$ and $W_2(p_x\rightarrow x)=1$, $W_2(x\rightarrow p_x)=-1$. Next we calculate weighted list ranking for $W_1$ and $W_2$ and  call it $\dOne$ and $\dTwo$, respectively. We have: 

\begin{itemize}
	\item $\preorder(x)=\dOne(p_x\rightarrow x)+1$
	\item $\preorder(x)+\size(x)-1=\dOne(x\rightarrow p_x)+1$
	\item $\level(x)=\dTwo(p_x\rightarrow x)$
\end{itemize}

Computing these values for $r$ is trivial.

We can now calculate $\inlabel$ as described in Section \ref{s:InlabelPreproc} using simple operations on bits.

Computing array $\ascendant$ also uses Euler tour representation. For each node $x\in T, x\neq r$ let $p_x:=\parent(x)$. When $\inlabel(x)=\inlabel(p_x)$ we put $W_3(p_x\rightarrow x)=W_3(x\rightarrow p_x)=0$. Otherwise let $i$ be the index of right-most ``1'' in $\inlabel(x)$. We assign $W_3(p_x\rightarrow x)=2^i$ and $W_3(x\rightarrow p_x)=-2^i$ with $\dThree$ defined in a way similar to above. Then $\ascendant(r)=\inlabel(r)$ and for $x\in T, x\neq r$ $\ascendant(x)=\dThree(p_x\rightarrow x)+\ascendant(r)$.

Having computed $\inlabel$ we compute $\head$ in a constant time per node.

\subsection{Optimizations}

We accept input as an array of parents for each node. We encode each edge from a parent to a child as a pair of 32bit integers into a single 64bit number, sort the array and then process it in $O(1)$ time per element to get the Euler tour representation.

We used open-source library called Moderngpu \cite{moderngpu:1} for sorting and scans (i.e. parallel prefix sum calculation).

Since list ranking is several times slower than scan, we calculate it once, rearrange edges according to ETR order and use scans for calculating weighted prefix sums.

A standard, pointer-jumping based list ranking algorithm works as follows: each node holds a pointer to next node and a temporary rank. Then in parallel each node moves its pointer to the next of the next and updates its rank. It is very similar approach to the one we used for calculating $\level$ in na\"ive GPU algorithm, it runs in $O(\lg{n})$ time with $O(n\lg n)$ work.

Instead of the standard algorithm we used an algorithm by Zheng Wei and Joseph JaJa \cite{ZW10:1}, because in our tests it ran over two times faster. It works by splitting the list at random locations and then assigning each sublist to a single processing thread. Each thread in parallel calculates ranks for a sublist by simply traversing it. Finally we calculate ranking of the sublists using a single thread and update ranks of each element. This algorithm has $O(n)$ work and the first phase leverages all computing threads of the GPU, which allows us to achieve good speed-up.

\section{Benchmarking Methodology}
\subsection{Tests}

We used generated tests for benchmarking. All of our programs accepted the same input format: the array of parents and the array of queries. We generated tests as follows: for each node successively we randomly selected a parent among all nodes that came before it, with first node as a root. Then, we shuffled all nodes to give them random ids while maintaining tree structure. Trees generated in such way have depth of $O(\lg{n})$.

To achieve deeper trees we simply put a constraint on how far back a parent of a node can be in the first phase of generation. For example, if a parent can be only $\sqrt{n}$ nodes back we get a tree with a depth of $O(\sqrt{n})$, if only one node back we get a path, and so on.

We chose to have the number of queries to be equal to the size of the tree, and generated all queries randomly among all nodes.

For each test we used 5 graphs generated with different seeds and then took averages of running times.

We did all testing on an NVIDIA GeForce GTX 980 graphics card. It has 2048 CUDA cores and 4GB of off-chip memory.

\subsection{Algorithms}
We use inlabel algorithm as our candidate for a theoretically fast solution that achieves good practical results. We compare it to a na\"ive GPU algorithm described in Section \ref{s:NaiveGPU}, and a baseline CPU implementation.

There are two main approaches for calculating the LCA on a CPU: an approach based on a tree decomposition, such as the inlabel algorithm, and an approach based on the reduction to the RMQ problem, such as the algorithm we mentioned before, described in a work of Bender and Farach-Colton \cite{BC00}.

We did a quick test of both approaches and found that while the inlabel algorithm runs preprocessing about two times slower than the RMQ based one, it answered queries about three times faster. We decided to have the number of queries to be equal number of nodes in a tree, and when we combined both preprocessing and query times, the inlabel algorithm was slightly faster. We used it as our CPU benchmark.

\section{Results}
\subsection{Main Benchmark}

\begin{figure}[h!]
\begin{subfigure}[b]{\textwidth}
\includegraphics[width=.5\textwidth]{1ShallowPre}%
\includegraphics[width=.5\textwidth]{1DeepPre}
\caption{Preprocessing time (left: shallow trees, right: deep trees)}
\end{subfigure}

\begin{subfigure}[b]{\textwidth}
\includegraphics[width=.5\textwidth]{1ShallowQue}%
\includegraphics[width=.5\textwidth]{1DeepQue}
\caption{Query time (left: shallow trees, right: deep trees)}
\end{subfigure}

\begin{subfigure}[b]{\textwidth}
\includegraphics[width=.5\textwidth]{1ShallowCombined}%
\includegraphics[width=.5\textwidth]{1DeepCombined}
\caption{Combined times (left: shallow trees, right: deep trees)}
\end{subfigure}

\caption{Main benchmark}
\label{f:MainBench}
\end{figure}



In all of our tests we decided to have the number of queries equal the tree size. We compare preprocessing time divided by tree size, time to answer queries divided by their number, and the combined times. We tested on trees of sizes from 1M (1 million nodes) to 64M. This is maximal size our implementation can handle on a GPU with 4GB of memory.

Figure \ref{f:MainBench} presents results of the main benchmark. On the left-hand side are results of shallow trees, where a parent is chosen without any restrictions. The depth of such trees is around $1.5\lg{n}$. On the right-hand side we have deeper trees, where a parent can only be chosen from the last 1000 nodes. This gave us trees of depth around $n/500$.

Preprocessing stage for inlabel algorithm takes around 5-6 times longer than preprocessing in na\"ive GPU algorithm, both for shallow and deep trees. In comparison with CPU, the inlabel GPU finishes preprocessing from 6 to 25 times faster, as tree sizes grow.

When we take a look at time per query, in shallow trees both inlabel and na\"ive GPU algorithms beat CPU times by a factor of around 100, with inlabel algorithm being about 3 times faster than the na\"ive one.

However, on deep trees it takes a very long time for na\"ive GPU algorithm to answers queries, which makes it unusable for such test cases. We set up a time limit of 5 minutes for each test - hence the missing values for na\"ive GPU algorithm on bigger tests. 

When we combine both preprocessing and query times, in shallow trees, inlabel algorithm gets much closer to na\"ive GPU algorithm and even beats it in the biggest tests. For deep trees the CPU and GPU versions of inlabel algorithm run similar to their respective times on shallow trees, with na\"ive GPU algorithm slowing down a lot. Even on the smallest of our tests it ran slower than the CPU algorithm.

\subsection{Tree depth}

Next, we investigate how na\"ive GPU and inlabel GPU algorithms behave with growing tree depth. We generate trees of 8M nodes and manipulate the constraint of how far back a parent of a node can be. This gives us trees of depth around $1.5\lg{n}$ for unrestricted parents and a path when parent can only be one node back.

\begin{figure}[h]
\begin{minipage}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{3Queries}
\caption{Query time by depth}
\end{minipage}%
\begin{minipage}{.5\textwidth}
\centering
\includegraphics[width=\textwidth]{3Combined}
\caption{Combined time by depth}
\end{minipage}%
\end{figure}

While query time for inlabel algorithm stays very similar when manipulating tree depth, the same cannot be said about the na\"ive GPU algorithm.

The average depth for random tree of 8M nodes is around 37, when a parent is chosen without restrictions. When we impose restrictions and depth grows above 45 it is no longer profitable to use na\"ive GPU algorithm over inlabel, provided the amount of queries is similar to the tree size.

\subsection{Batch sizes}

Some of use cases of LCA algorithms profit from small latency between providing a query and getting the answer for that query. This is obviously not the best scenario for GPU algorithms as they get faster when process more data at once.

We tested the following scenario: for inlabel GPU algorithm we take a batch of the input queries, copy it to the GPU device, process them and copy answers back. We used trees of 8M nodes in this test.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{2BatchSize}
\caption{Query time by batch size}
\end{figure}

As batch size grows, the average query time for inlabel GPU algorithm falls. It is faster than answering queries on CPU with batch size around 150 and achieves results close to best with batch size around 10,000. It is expected, as the number of processing cores on our GPU equals 2056 and overall time profits from computing a couple of queries on a single core.

\section{Conclusions}

We have shown that carefully chosen and implemented theoretically optimal PRAM algorithm for calculating LCA can achieve good results on a modern GPU machine. It is on average only a little bit slower than its na\"ive alternative but doesn't suffer in worst case scenarios.

If one can afford processing queries in a reasonably sized batches, using GPU for this task pays off.


\pagebreak
\bibliography{bbl}
\bibliographystyle{alpha}

\end{document}
